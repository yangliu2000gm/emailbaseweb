﻿using Blazorise;
using Blazorise.DataGrid;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using System.Threading;

using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using Blazorise.Bootstrap;
using System.Reflection.Emit;
using Microsoft.AspNetCore.Mvc;
using emailbase2023.DB;
using System.Collections.Generic;
using System.Xml;

namespace emailbase2023.Components
{
    public partial class TaskStatus
    {
        List<ModelTasks> dataModelsTask = new List<ModelTasks>();


        protected override async Task OnInitializedAsync()
        {

            await RefreshData();

        }

        async Task RefreshData()
        {

            List<List<string>> list = new List<List<string>>();

            XmlDocument originalXml = new XmlDocument();
            originalXml.Load("UserTasks.xml");

            foreach (XmlNode node in originalXml.GetElementsByTagName("Task"))
            {
                if (node.Attributes["InUse"] == null)
                    continue;
                string sInUse = node.Attributes["InUse"].Value.ToString();
                if (sInUse == "Y")
                {
                    List<string> ls = new List<string>();

                    ls.Add(node.Attributes["Name"].Value.ToString());
                    ls.Add(node["Server"].InnerText.ToString());
                    ls.Add(node["DBName"].InnerText.ToString());


                    ls.Add(node["ServerUserName"].InnerText.ToString());
                    ls.Add(node["ServerPwd"].InnerText.ToString());

                    ls.Add(node["EmailTo"].InnerText.ToString());

                    ls.Add(node["SPName"].InnerText.ToString());
                    ls.Add(node["cronExpression"].InnerText.ToString());
                    ls.Add(node["LastRunTime"].InnerText.ToString());
                    ls.Add(node["NextRunTime"].InnerText.ToString());

                    ls.Add("stoping");
                    list.Add(ls);

                    //if (node["started"].InnerText.ToString() == "false")
                    //    dIfStarted.Add(node.Attributes["Name"].Value.ToString(), false);
                    //else
                    //    dIfStarted.Add(node.Attributes["Name"].Value.ToString(), true);
                }
            }




            StateHasChanged();
        }


    }
}
