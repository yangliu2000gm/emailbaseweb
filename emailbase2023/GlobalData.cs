﻿using NLog.Web;

namespace emailbase2023
{
    public class GlobalData
    {
        static public NLog.Logger logger;
        static public IConfiguration config;

        static public bool logined = false;
        static public string loginUser = "";
        public static void Logout()
        {
            logined = false;
            loginUser = "";
        }
        static public void Init()
        {
            config = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
            logger = NLogBuilder.ConfigureNLog("NLog.config").GetCurrentClassLogger();//.GetLogger("normal");
            //string ConnSQL = config["Config:SQLConn"].ToString();

            //DB.DBFactory.InitConnectString(ConnSQL);

        }

    }
}
