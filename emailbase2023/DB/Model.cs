﻿namespace emailbase2023.DB
{
    public class ModelTasks
    {

        public string task_name { set; get; } = "";
        public string sp_name { set; get; } = "";
        public string taskCronExpress { set; get; } = "";
  
        public DateTime lastrun_time { set; get; } = DateTime.Today;
        public DateTime nextrun_time { set; get; } = DateTime.Today;


    }

    public class sqlTask
    {
        public string TaskName;

        public string ServerIPOrName;
        public string DBName;
        public string ServerUserName;
        public string ServerPwd;
        public string StoreProcName;
        public string TaskCronExpression;
        public string EmailTitle;
        public string EmailFrom;
        public string EmailTo;
        public string EmailCC;
        public DateTime TaskLastRunTime;
        public DateTime TaskNextRunTime;

        public bool started = false;

    }

}
