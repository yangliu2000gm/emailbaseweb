﻿using System.Data.SqlClient;
using System.Data;

namespace emailbase2023.DB
{
    public class DBCommands2
    {
        public string mConnectString = "";
        //private  int ConnectTimeout = 300;
        public int ExecuteTimeout = 300;
        public int LastErrorCode = 0;
        public string LastError = "";

        public void InitConnectString(string s)
        {
            mConnectString = s;
        }

        public string ConnectString
        {

            get
            {
                if (mConnectString == "")
                    mConnectString = "";
                //mConnectString = soloRatePlanImporter.Properties.Settings.Default.SmartBillingConnectionString;
                mConnectString = mConnectString.Replace("Provider=SQLOLEDB;", "");
                return mConnectString;
            }
        }

        private void AddParam(SqlCommand sqlCommand, string paramName, SqlDbType type, int size, int prec, int scale, ParameterDirection direction, object value)
        {
            SqlParameter sqlParams = new SqlParameter();
            sqlParams.ParameterName = paramName;
            sqlParams.SqlDbType = type;
            sqlParams.Size = size;
            if (type == SqlDbType.Decimal)
            {
                sqlParams.Precision = (byte)prec;
                sqlParams.Scale = (byte)scale;
            }
            sqlParams.Direction = direction;
            sqlCommand.Parameters.Add(sqlParams);
            if ((direction == ParameterDirection.Output) && (value == DBNull.Value))
                return;
            sqlCommand.Parameters[paramName].Value = value;
        }

        private int get_int_value(object data)
        {
            return ((data == null) || (data == DBNull.Value)) ? 0 : Convert.ToInt32(data);
        }

        private string get_string_value(object data)
        {
            return ((data == null) || (data == DBNull.Value)) ? "" : Convert.ToString(data);
        }

        private double get_double_value(object data)
        {
            return ((data == null) || (data == DBNull.Value)) ? 0 : Convert.ToDouble(data);
        }

        private decimal get_decimal_value(object data)
        {
            return ((data == null) || (data == DBNull.Value)) ? 0 : Convert.ToDecimal(data);
        }

        private DateTime get_DateTime_value(object data)
        {
            return ((data == null) || (data == DBNull.Value)) ? DateTime.MinValue : Convert.ToDateTime(data);
        }

        private TimeSpan get_TimeSpan_value(object data)
        {
            return ((data == null) || (data == DBNull.Value)) ? TimeSpan.Parse("00:00:00") : (TimeSpan)data;
        }

        private object get_object_value(object data)
        {
            return data;
        }



        public bool sp_query_sp_and_get_result_table
        (
            string spName,
            ref DataTable dt
        )
        {
            dt = new DataTable();
            SqlDataReader reader = null;
            SqlConnection conn = null;
            SqlCommand sqlCmd = null;
            try
            {
                try
                {
                    conn = new SqlConnection(ConnectString);
                    conn.Open();
                    if (conn.State != ConnectionState.Open)
                    {
                        LastErrorCode = -1;
                        LastError = "Connect database error.";
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    LastErrorCode = -1;
                    LastError = "Connect database error. " + ex.ToString();
                    return false;
                }

                try
                {
                    sqlCmd = new SqlCommand(spName, conn);
                    sqlCmd.CommandTimeout = ExecuteTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;


                    //sqlCmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 4);
                    //sqlCmd.Parameters["@RETURN_VALUE"].Direction = ParameterDirection.ReturnValue;
                    reader = sqlCmd.ExecuteReader();
                    if (reader != null)
                    {
                        dt.Load(reader);
                    }


                    //int _temp = Convert.ToInt32(sqlCmd.Parameters["@RETURN_VALUE"].Value.ToString());
                    return true;
                }
                catch (Exception ex)
                {
                    LastErrorCode = -1;
                    LastError = "Execute sp_query_sp_and_get_result_table error. " + ex.ToString();
                    return false;
                }
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
                if (sqlCmd != null)
                    sqlCmd.Dispose();
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }
        //2019-03-05

        public bool sp_query_sp_and_get_result_table_using_ds
        (
            string spName,
            ref DataSet ds
)
        {
            ds = new DataSet();
            SqlDataReader reader = null;
            SqlConnection conn = null;
            SqlCommand sqlCmd = null;
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                try
                {
                    conn = new SqlConnection(ConnectString);
                    conn.Open();
                    if (conn.State != ConnectionState.Open)
                    {
                        LastErrorCode = -1;
                        LastError = "Connect database error.";
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    LastErrorCode = -1;
                    LastError = "Connect database error. " + ex.ToString();
                    return false;
                }

                try
                {
                    sqlCmd = new SqlCommand(spName, conn);
                    sqlCmd.CommandTimeout = ExecuteTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;


                    //reader = sqlCmd.ExecuteReader();
                    //if (reader != null)
                    //{
                    //    dt.Load(reader);
                    //}

                    da = new SqlDataAdapter(sqlCmd);
                    da.Fill(ds);


                    //int _temp = Convert.ToInt32(sqlCmd.Parameters["@RETURN_VALUE"].Value.ToString());
                    return true;
                }
                catch (Exception ex)
                {
                    LastErrorCode = -1;
                    LastError = "Execute sp_query_sp_and_get_result_table error. " + ex.ToString();
                    return false;
                }
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
                if (sqlCmd != null)
                    sqlCmd.Dispose();
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }

    }
}
