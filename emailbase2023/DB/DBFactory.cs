﻿using System.Collections;
using System.Data.SqlClient;

namespace emailbase2023.DB
{
    public class DBFactory
    {
        public static Hashtable connectStrings = new Hashtable();

        public static void InitConnectString(string connectString)
        {
            if (!connectStrings.ContainsKey("default"))
            {
                connectStrings.Add("default", connectString);
            }
        }

        public static void InitConnectString(string server, string connectString)
        {
            if (!connectStrings.ContainsKey(server))
            {
                connectStrings.Add(server, connectString);
            }
        }

        public static SqlConnection getConnect()
        {
            return getConnect("default");
        }

        public static SqlConnection getConnect(string server)
        {
            SqlConnection conn = null;
            string connectString = "";
            if (connectStrings.ContainsKey(server))
            {
                try
                {
                    connectString = connectStrings[server] as string;
                    conn = new SqlConnection(connectString);
                    conn.Open();
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return conn;
        }

    }
}
