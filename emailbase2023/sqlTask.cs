﻿namespace emailbase2023
{
    class sqlTask
    {
        public string TaskName;

        public string ServerIPOrName;
        public string DBName;
        public string ServerUserName;
        public string ServerPwd;
        public string StoreProcName;
        public string TaskCronExpression;
        public string EmailTitle;
        public string EmailFrom;
        public string EmailTo;
        public string EmailCC;
        public DateTime TaskLastRunTime;
        public DateTime TaskNextRunTime;

        public bool started = false;

    }
}
