﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using NCrontab;
using System.Threading;
using System.IO;

using System.Net.Mail;
using System.Net;
using Quartz;
using Quartz.Impl;
using Quartz.Core;
using MimeKit;
using MailKit;


namespace emailbase2023
{
    public class TimeService : IHostedService, IDisposable
    {

        public static bool IsRunning = true;
        private int executionCount = 0;
        private Timer? _timer = null;
        List<sqlTask> LTasks = new List<sqlTask>();
        private readonly ILogger<TimeService> _logger;


        public TimeService(ILogger<TimeService> logger)
        {
            _logger = logger;
            IsRunning = true;
            ReloadTasks();

        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            //_timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(300));

            XmlDocument originalXml = new XmlDocument();
            originalXml.Load("UserTasks.xml");



            XmlNodeList xnList = originalXml.SelectNodes("/UserDefTasks/Task/started");
            foreach (XmlNode xn in xnList)
            {
                xn.InnerText = "true";
            }
            originalXml.Save("UserTasks.xml");

            int iCronID = 1;
            foreach (sqlTask t in LTasks)
            {
                string[] SPsList = t.StoreProcName.Split(',');
                if (SPsList.Length != 1)    // not only one SP 
                {
                    string[] TitlesList = t.EmailTitle.Split(',');
                    string[] EmailTosList = t.EmailTo.Split(',');

                    for (int i = 0; i < SPsList.Length; i++)
                    {
                        // construct a scheduler factory
                        ISchedulerFactory schedFact = new StdSchedulerFactory();

                        // get a scheduler
                        IScheduler sched = schedFact.GetScheduler().GetAwaiter().GetResult();
                        sched.Start();

                        // define the job and tie it to our HelloJob class
                        IJobDetail job = JobBuilder.Create<callSPandSendEmail>()
                            .WithIdentity("emailJob" + iCronID.ToString(), "group2")
                            .UsingJobData("taskname", t.TaskName)
                            .UsingJobData("serverIP", t.ServerIPOrName)
                            .UsingJobData("DBname", t.DBName)
                            .UsingJobData("serverusername", t.ServerUserName)
                            .UsingJobData("serverpwd", t.ServerPwd)
                            .UsingJobData("spname", SPsList[i])
                            .UsingJobData("emailtitle", TitlesList[i])
                            .UsingJobData("emailto", EmailTosList[i])
                            //.UsingJobData("taskname", "task1")
                            //.UsingJobData("serverIP", "ip1")
                            //.UsingJobData("DBname", "db1")
                            //.UsingJobData("serverusername", "server1")
                            //.UsingJobData("serverPWD", "pwd1")
                            .Build();


                        ITrigger trigger = TriggerBuilder.Create()
                          .WithIdentity("emailTrigger" + iCronID.ToString(), "group2")
                          .StartNow()
                          .WithCronSchedule(t.TaskCronExpression)
                          .ForJob(job)
                          .Build();



                        sched.ScheduleJob(job, trigger);
                        iCronID++;
                    }

                }
                else
                {
                    // construct a scheduler factory
                    ISchedulerFactory schedFact = new StdSchedulerFactory();

                    // get a scheduler
                    IScheduler sched = schedFact.GetScheduler().GetAwaiter().GetResult();
                    sched.Start();

                    // define the job and tie it to our HelloJob class
                    IJobDetail job = JobBuilder.Create<callSPandSendEmail>()
                        .WithIdentity("emailJob" + iCronID.ToString(), "group2")
                        .UsingJobData("taskname", t.TaskName)
                        .UsingJobData("serverIP", t.ServerIPOrName)
                        .UsingJobData("DBname", t.DBName)
                        .UsingJobData("serverusername", t.ServerUserName)
                        .UsingJobData("serverpwd", t.ServerPwd)
                        .UsingJobData("spname", t.StoreProcName)
                        .UsingJobData("emailtitle", t.EmailTitle)
                        .UsingJobData("emailto", t.EmailTo)
                        //.UsingJobData("taskname", "task1")
                        //.UsingJobData("serverIP", "ip1")
                        //.UsingJobData("DBname", "db1")
                        //.UsingJobData("serverusername", "server1")
                        //.UsingJobData("serverPWD", "pwd1")
                        .Build();

                    ITrigger trigger = TriggerBuilder.Create()
                      .WithIdentity("emailTrigger" + iCronID.ToString(), "group2")
                      .StartNow()
                      .WithCronSchedule(t.TaskCronExpression)
                      .ForJob(job)
                      .Build();
                    sched.ScheduleJob(job, trigger);
                    iCronID++;
                }

            }






            return Task.CompletedTask;

        }


        private void DoWork(object? state)
        {
            XmlDocument originalXml = new XmlDocument();
            originalXml.Load("UserTasks.xml");

            XmlNodeList xnList = originalXml.SelectNodes("/UserDefTasks/Task/started");
            foreach (XmlNode xn in xnList)
            {
                xn.InnerText = "true";
            }
            originalXml.Save("UserTasks.xml");

            int iCronID = 1;
            foreach (sqlTask t in LTasks)
            {
                string[] SPsList = t.StoreProcName.Split(',');
                if (SPsList.Length != 1)    // not only one SP 
                {
                    string[] TitlesList = t.EmailTitle.Split(',');
                    string[] EmailTosList = t.EmailTo.Split(',');

                    for (int i = 0; i < SPsList.Length; i++)
                    {
                        // construct a scheduler factory
                        ISchedulerFactory schedFact = new StdSchedulerFactory();

                        // get a scheduler
                        IScheduler sched = schedFact.GetScheduler().GetAwaiter().GetResult();
                        sched.Start();

                        // define the job and tie it to our HelloJob class
                        IJobDetail job = JobBuilder.Create<callSPandSendEmail>()
                            .WithIdentity("emailJob" + iCronID.ToString(), "group2")
                            .UsingJobData("taskname", t.TaskName)
                            .UsingJobData("serverIP", t.ServerIPOrName)
                            .UsingJobData("DBname", t.DBName)
                            .UsingJobData("serverusername", t.ServerUserName)
                            .UsingJobData("serverpwd", t.ServerPwd)
                            .UsingJobData("spname", SPsList[i])
                            .UsingJobData("emailtitle", TitlesList[i])
                            .UsingJobData("emailto", EmailTosList[i])
                            //.UsingJobData("taskname", "task1")

                            //.UsingJobData("serverIP", "ip1")
                            //.UsingJobData("DBname", "db1")
                            //.UsingJobData("serverusername", "server1")
                            //.UsingJobData("serverPWD", "pwd1")
                            .Build();

         
                        ITrigger trigger = TriggerBuilder.Create()
                          .WithIdentity("emailTrigger" + iCronID.ToString(), "group2")
                          .StartNow()
                          .WithCronSchedule(t.TaskCronExpression)
                          .ForJob(job)
                          .Build();



                        sched.ScheduleJob(job, trigger);
                        iCronID++;
                    }

                }
                else
                {
                    // construct a scheduler factory
                    ISchedulerFactory schedFact = new StdSchedulerFactory();

                    // get a scheduler
                    IScheduler sched = schedFact.GetScheduler().GetAwaiter().GetResult();
                    sched.Start();

                    // define the job and tie it to our HelloJob class
                    IJobDetail job = JobBuilder.Create<callSPandSendEmail>()
                        .WithIdentity("emailJob" + iCronID.ToString(), "group2")
                        .UsingJobData("taskname", t.TaskName)
                        .UsingJobData("serverIP", t.ServerIPOrName)
                        .UsingJobData("DBname", t.DBName)
                        .UsingJobData("serverusername", t.ServerUserName)
                        .UsingJobData("serverpwd", t.ServerPwd)
                        .UsingJobData("spname", t.StoreProcName)
                        .UsingJobData("emailtitle", t.EmailTitle)
                        .UsingJobData("emailto", t.EmailTo)
                        //.UsingJobData("taskname", "task1")
                        //.UsingJobData("serverIP", "ip1")
                        //.UsingJobData("DBname", "db1")
                        //.UsingJobData("serverusername", "server1")
                        //.UsingJobData("serverPWD", "pwd1")
                        .Build();

                    ITrigger trigger = TriggerBuilder.Create()
                      .WithIdentity("emailTrigger" + iCronID.ToString(), "group2")
                      .StartNow()
                      .WithCronSchedule(t.TaskCronExpression)
                      .ForJob(job)
                      .Build();
                    sched.ScheduleJob(job, trigger);
                    iCronID++;
                }

            }


        }


        private void ReloadTasks()
        {
            LTasks.Clear();

            XmlDocument originalXml = new XmlDocument();
            originalXml.Load("UserTasks.xml");


            foreach (XmlNode node in originalXml.GetElementsByTagName("Task"))
            {
                if (node.Attributes["InUse"] == null)
                    continue;
                string sInUse = node.Attributes["InUse"].Value.ToString();
                if (sInUse == "Y")
                {
                    sqlTask t = new sqlTask();
                    t.TaskName = node.Attributes["Name"].Value.ToString();
                    t.ServerIPOrName = node["Server"].InnerText.ToString();
                    t.ServerUserName = node["ServerUserName"].InnerText.ToString();
                    t.ServerPwd = node["ServerPwd"].InnerText.ToString();
                    t.StoreProcName = node["SPName"].InnerText.ToString();
                    //t.ServerPort = node["ServerPort"].InnerText.ToString();
                    t.DBName = node["DBName"].InnerText.ToString();

                    t.TaskCronExpression = node["cronExpression"].InnerText.ToString();
                    t.EmailTitle = node["EmailTitle"].InnerText.ToString();

                    //t.TaskNextRunTime = CalculateTaskNextRunTime( t);    190226
                    t.EmailTo = node["EmailTo"].InnerText.ToString();


                    if (node["started"].InnerText.ToString() == "false")
                        t.started = false;
                    else
                        t.started = true;

                    LTasks.Add(t);
                }
            }

        }    // end of initTasks


        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }


    }

    public class callSPandSendEmail : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            JobDataMap dataMap = context.JobDetail.JobDataMap;
               
            string sTaskName = dataMap.GetString("taskname");
            string sServerIP = dataMap.GetString("serverIP");
            string sDBName = dataMap.GetString("DBname");
            string sUserName = dataMap.GetString("serverusername");
            string sPWD = dataMap.GetString("serverpwd");
            string sSpName = dataMap.GetString("spname");
            string sEmailTille = dataMap.GetString("emailtitle");
            string sEmailTo = dataMap.GetString("emailto");


            DateTime TaskLastRunTime = DateTime.Now;

            //Console.WriteLine(dataMap.GetString("taskname"));
            //Console.WriteLine(dataMap.GetString("serverIP"));
            //Console.WriteLine(dataMap.GetString("emailtitle"));
            //Console.WriteLine(dataMap.GetString("emailto"));

            //combine connection string 
            //example 
            //Data Source=hk.worldhubcom.cn,1026;Initial Catalog=MyCatcher;Persist Security Info=True;User ID=sa;Password=Passw0rdPassw0rd123
            string sRecord = sTaskName;

            string sConnStr = @"Data Source=" + sServerIP + @";Initial Catalog=" + sDBName + @";Persist Security Info=True;User ID=" + sUserName + @";Password=" + sPWD;

            //Logger.Info(sTaskName + " connecting DB ");

            DBCommands2 ds2 = new DBCommands2();

            ds2.InitConnectString(sConnStr);
            //call sp and get result table


            //Logger.Info(sTaskName + " excuting SP ");


            //DataTable dt = new DataTable();
            DataSet ds = new DataSet();



            sRecord = sRecord + @"," + DateTime.Now.ToString();

            //            if (ds2.sp_query_sp_and_get_result_table(t.StoreProcName, ref dt) == false)
            //if (ds2.sp_query_sp_and_get_result_table(sSpName, ref dt) == false)
            if (ds2.sp_query_sp_and_get_result_table_using_ds(sSpName, ref ds) == false)
            {


                sRecord = sRecord + @",,exec sp fail";

                using (StreamWriter sw = File.AppendText("record.txt"))
                {
                    sw.WriteLine(sRecord);
                }

                return null;

            }


            foreach (DataTable dt in ds.Tables)
            {

                //now result already in dt , first grnerate a nice html page and email it 

                //Logger.Info(sTaskName + " sending email ");

                //SmtpClient client = new SmtpClient("mail.worldhubcom.com", 465)
                //{
                //    //Credentials = new NetworkCredential("alert@worldhubcom.com", "alert5566"),
                //    Credentials = new NetworkCredential("didsupport@worldhubcom.com", "z2O?5jCkyzHt"),
                //    EnableSsl = true
                //};





                string sEmailTilleTop = "";
                if (sEmailTille.Contains("svr 24"))
                    sEmailTilleTop = "svr 24";

                if (sEmailTille.Contains("Main Bill Server"))
                    sEmailTilleTop = "Bill";

                if (sEmailTille.Contains("SBA1"))
                    sEmailTilleTop = "SBA1";


                if (sEmailTille.Contains("SBA2"))
                    sEmailTilleTop = "SBA2";


                MailAddress from = new MailAddress(@"alert@worldhubcom.com", @"alert(" + sEmailTilleTop + @")");
                //MailAddress to = new MailAddress(@"mzcw2011@sina.cn", "receiverName");



                //MailAddress to = new MailAddress(@"yang.liu@worldhubcom.com", "receiverName");

                //MailMessage myMail = new System.Net.Mail.MailMessage();
                MimeMessage myMail = new MimeKit.MimeMessage();

                myMail.Sender=new MimeKit.MailboxAddress("alert","alert@worldhubcom.com");

                myMail.From.Add(new MailboxAddress("alert@worldhubcom.com", "alert(" + sEmailTilleTop + @")"));
                string scontent = "";
               

                foreach (var address in sEmailTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    myMail.To.Add(new MailboxAddress("customer", address));
                }

                //            // set subject and encoding
                //            myMail.Subject = "Auto send email of route 's perfermance at sometime ";
                //            myMail.SubjectEncoding = System.Text.Encoding.UTF8;

                //            myMail.Body = @"<table style=""margin: 0;
                //  padding: 0;
                //  border: 0;
                //  font-size: 100%;
                //  font: inherit;border-collapse: collapse; border-spacing: 0;"">";
                //            myMail.Body = myMail.Body + @"<tr style=""background-color: #FFFFFF; border: solid thin"" ><td style=""border:solid  1px""> Title </td> <td>" + t.TaskName + @"</td>" + @"</tr>";
                //            myMail.Body = myMail.Body + @"<tr style=""background-color: #F2F2F2; border: solid thin"" ><td style=""border:solid  1px""> Server </td> <td>" + t.ServerIPOrName + @"</td>" + @"</tr>";
                //            myMail.Body = myMail.Body + @"<tr style=""background-color: #FFFFFF; border: solid thin"" ><td style=""border:solid  1px""> Retrieve Time </td> <td>" + t.TaskNextRunTime.ToString() + @"</td>" + @"</tr>";
                //            myMail.Body = myMail.Body + @"<tr style=""background-color: #F2F2F2; border: solid thin"" ><td style=""border:solid  1px""> Subscriber </td> <td>" + t.EmailTo + @"</td>" + @"</tr>";

                //            myMail.Body = myMail.Body + @"</table>";

                myMail.Subject = sEmailTille;

                //            myMail.Body = myMail.Body + @"<p>Hi:</p>" + "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + t.EmailTitle + " at time " + t.TaskNextRunTime + "</p> <hr>" + @"<p>From server:" + t.ServerIPOrName + @"</p><p>Receiver: " + t.EmailTo + @"</p>" + @"<table style=""font-size: 13px; font-family:Calibri; margin: 0;
                //  padding: 0;
                //  border: 0;
                //  font-size: 100%;
                //  font: inherit;border-collapse: collapse; border-spacing: 0;"">";



                scontent = scontent + @"<p style=""font-size: 15px; font-family:Calibri;"">Retrieve Time:" + @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + TaskLastRunTime.ToString("dd'/'MM'/'yyyy HH:mm") + @"</p>" + @"<p style=""font-size: 15px; font-family:Calibri;"">Report Name:" + @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + sEmailTille + "</p>" + @"<p style=""font-size: 15px; font-family:Calibri;"">Server Name:" + @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + sServerIP + @"</p><p style=""font-size: 15px; font-family:Calibri;"">Receiver:" + @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + sEmailTo + @"</p>" + @"<table style=""font-size: 13px; font-family:Calibri; margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;border-collapse: collapse; border-spacing: 0;"">";



                //            myMail.Body = myMail.Body + @"<thead>
                //		<tr >";      //#ED7D31
                //            for (int i = 0; i < dt.Columns.Count; i++)
                //            {
                //                myMail.Body = myMail.Body + @"<td style=""background-color: #ED7D31;color:#fff>" + dt.Columns[i].Caption +
                //                    @"</td>";
                //            }

                //            myMail.Body = myMail.Body + @"</tr>";
                //            myMail.Body = myMail.Body + @"</thead>";

                scontent = scontent + @"
		<tr >";      //#ED7D31
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    scontent = scontent + @"<td style=""background-color: #ED7D31;color:#fff"">" + dt.Columns[i].Caption +
                        @"</td>";
                }

                scontent = scontent + @"</tr>";




                //            for (int i = 0; i < dt.Columns.Count; i++)
                //            {
                //                myMail.Body = myMail.Body + @"<th style="" 
                //  padding: 12px 30px;
                //  padding-left: 42px;"">" + dt.Columns[i].Caption +
                //                    @"</th>";

                //            }
                //            myMail.Body = myMail.Body + @"</tr>";

                int iOdd = 2;

                if (dt.Rows.Count == 0)
                    return null;

                foreach (DataRow row in dt.Rows)
                {
                    if (iOdd % 2 == 0)
                    {
                        scontent = scontent + @"<tr style=""  
  padding: 12px 30px;
  padding-left: 42px; background-color: #FBE4D5;"">";
                    }
                    else
                    {
                        scontent = scontent + @"<tr style=""  
  padding: 12px 30px;
  padding-left: 42px; background-color: #FFFFFF;"">";
                    }

                    iOdd = iOdd + 1;

                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        //                    myMail.Body = myMail.Body + @"<td style = ""text-align:center;
                        //  padding: 15px 10px;border-bottom: 1px solid #c0c0c0; color: black;"">" + row[i].ToString() +
                        //                        @"</td>";


                        if (dt.Columns[i].ColumnName.Contains("CustomerMinutes"))    //only for SmartBilling_Report 
                        {

                            if (row[i] == null || string.IsNullOrEmpty(row[i].ToString()))
                            {
                                return null;
                            }

                        }


                        //number align to right  
                        double ifNumbericValue;
                        if (Double.TryParse(row[i].ToString(), out ifNumbericValue))
                        {
                            ifNumbericValue = Math.Round(ifNumbericValue, 2);

                            if (dt.Columns[i].ColumnName.Contains("DifferentBy"))
                            {

                               
                                if (ifNumbericValue >= 0)
                                    return null;
                                else
                                {
                                    //                            scontent = scontent + @"<td align=""right"" style = ""border-top:none;border-left:solid #F4B083 1.0pt;border-bottom:solid #F4B083 1.0pt;border-right:none;padding:0in 5.4pt 0in 5.4pt"">" + String.Format("Value: {0:P2}.", ifNumbericValue) +
                                    //@"</td>";

                                    scontent = scontent + @"<td align=""right"" style = ""border-top:none;border-left:solid #F4B083 1.0pt;border-bottom:solid #F4B083 1.0pt;border-right:none;padding:0in 5.4pt 0in 5.4pt"">" + ifNumbericValue.ToString() +
      @"</td>";
                                }

                            }
                            else
                            {
                                scontent = scontent + @"<td align=""right"" style = ""border-top:none;border-left:solid #F4B083 1.0pt;border-bottom:solid #F4B083 1.0pt;border-right:none;padding:0in 5.4pt 0in 5.4pt"">" + ifNumbericValue.ToString() +
        @"</td>";
                            }


                        }
                        else if (dt.Columns[i].ColumnName.Contains("Date") && (dt.Columns[i].DataType == typeof(DateTime)))    //if date column  , do not show time
                        {
                            DateTime dateValue;
                            string s2;
                            if (DateTime.TryParse(row[i].ToString(), out dateValue))
                            {
                                scontent = scontent + @"<td align=""left"" style = ""border-top:none;border-left:solid #F4B083 1.0pt;border-bottom:solid #F4B083 1.0pt;border-right:none;padding:0in 5.4pt 0in 5.4pt"">" + dateValue.Day.ToString() + @"/" + dateValue.Month.ToString() + @"/" + dateValue.Year.ToString() +
            @"</td>";
                            }
                            else
                            {
                                scontent = scontent + @"<td align=""left"" style = ""border-top:none;border-left:solid #F4B083 1.0pt;border-bottom:solid #F4B083 1.0pt;border-right:none;padding:0in 5.4pt 0in 5.4pt"">" + row[i].ToString() +
                                           @"</td>";
                            }

                        }

                        else
                        {
                            scontent = scontent + @"<td align=""left"" style = ""border-top:none;border-left:solid #F4B083 1.0pt;border-bottom:solid #F4B083 1.0pt;border-right:none;padding:0in 5.4pt 0in 5.4pt"">" + row[i].ToString() +
        @"</td>";

                        }

                        // if column name include date  ,  only show date , not include time 


                    }

                    scontent = scontent + @"</tr>";
                }

                scontent = scontent + @"</table>";

                // done email table 

                //myMail.BodyEncoding = System.Text.Encoding.UTF8;

                //myMail.IsBodyHtml = true;



                sRecord = sRecord + @"," + DateTime.Now.ToString();
                try
                {

                    //SmtpClient client = new SmtpClient("mail.worldhubcom.com", 465)
                    //{
                    //    //Credentials = new NetworkCredential("alert@worldhubcom.com", "alert5566"),
                    //    Credentials = new NetworkCredential("didsupport@worldhubcom.com", "z2O?5jCkyzHt"),
                    //    EnableSsl = true
                    //};


                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("mail.worldhubcom.com", 465, true);
                        client.AuthenticationMechanisms.Remove("XOAUTH2");
                        client.Authenticate("didsupport@worldhubcom.com", "z2O?5jCkyzHt");
                    
                        myMail.Body = new TextPart("html") { Text = scontent };
                        client.Send(myMail);
                        client.Disconnect(true);
                      
                    }




                    sRecord = sRecord + @"," + "success";

                    using (StreamWriter sw = File.AppendText("record.txt"))
                    {
                        sw.WriteLine(sRecord);
                    }


                    //t.TaskLastRunTime = DateTime.Now;

                }
                catch (Exception sendemailex)
                {

                    //globalda.Error(sTaskName + @": " + sendemailex.Message);


                    sRecord = sRecord + @"," + "fail to send email";

                    using (StreamWriter sw = File.AppendText("record.txt"))
                    {
                        sw.WriteLine(sRecord);
                    }


                    return null;

                }

                //Logger.Info(sTaskName + " exec succuess ");
            } // end for each dt 


            return null;


        }
    }


}